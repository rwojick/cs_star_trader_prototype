*comment : global.txt

*label init_vars

*set ship_speed (2 / 7)
*set ship_max_tons 30
*set star_min_dist 15
*set bid_rounds 3
*set profit_margin 36
*set curr_day 1
*set start_year 2070
*set curr_year {start_year}
*set star_dev_inc 1.25
*set delay_chance 0.1
*set is_restart false
*set num_players 0
*set num_ships 0
*set num_stars 0
*set game_length 0

*gosub init_def_prod_vals

*return

*label init_def_prod_vals

*comment : TODO : Below still valid?
*comment : BUG : Need to add leading and trailing 0s

*comment : *csx_array_set prod_slope (0 - 2) (0 - 5) ("-0.025" "-0.050" "-0.025" 0.000 "-0.025" "-0.025" 0.000 0.100 0.100 "-0.025" 0.100 0.000 0.100 0.200 0.100 0.100 "-0.025" 0.000)
*comment : *csx_checksum 5bebb55a18ca6615d597aef9e4eafa40
*comment : *csx_generated_code_start : Please do not remove unless cleaning project
*set prod_slope[0][0] "-0.025"
*set prod_slope[0][1] "-0.050"
*set prod_slope[0][2] "-0.025"
*set prod_slope[0][3] 0.000
*set prod_slope[0][4] "-0.025"
*set prod_slope[0][5] "-0.025"
*set prod_slope[1][0] 0.000
*set prod_slope[1][1] 0.100
*set prod_slope[1][2] 0.100
*set prod_slope[1][3] "-0.025"
*set prod_slope[1][4] 0.100
*set prod_slope[1][5] 0.000
*set prod_slope[2][0] 0.100
*set prod_slope[2][1] 0.200
*set prod_slope[2][2] 0.100
*set prod_slope[2][3] 0.100
*set prod_slope[2][4] "-0.025"
*set prod_slope[2][5] 0.000
*comment : *csx_generated_code_end : Please do not remove unless cleaning project

*comment : *csx_array_set prod_base (0 - 2) (0 - 5) (1.000 1.500 0.500 0.750 0.750 0.750 "-0.250" "-0.250" "-0.250" 0.000 "-0.500" 0.500 0.000 "-0.500" 0.000 0.500 1.500 0.000)
*comment : *csx_checksum 255438fbac87f747e17950ccf312fab1
*comment : *csx_generated_code_start : Please do not remove unless cleaning project
*set prod_base[0][0] 1.000
*set prod_base[0][1] 1.500
*set prod_base[0][2] 0.500
*set prod_base[0][3] 0.750
*set prod_base[0][4] 0.750
*set prod_base[0][5] 0.750
*set prod_base[1][0] "-0.250"
*set prod_base[1][1] "-0.250"
*set prod_base[1][2] "-0.250"
*set prod_base[1][3] 0.000
*set prod_base[1][4] "-0.500"
*set prod_base[1][5] 0.500
*set prod_base[2][0] 0.000
*set prod_base[2][1] "-0.500"
*set prod_base[2][2] 0.000
*set prod_base[2][3] 0.500
*set prod_base[2][4] 1.500
*set prod_base[2][5] 0.000
*comment : *csx_generated_code_end : Please do not remove unless cleaning project

*return

*label init_star_names

*temp i 0
*temp t ""

*comment : *csx_array_set star_names (0 - 14) ("Sol" "York" "Boyd" "Ivan" "Reef" "Hook" "Stan" "Task" "Sink" "Sand" "Quin" "Gaol" "Kirk" "Kris" "Fate")
*comment : *csx_checksum b9c47f2ca59d047a2716f0d1fb300301
*comment : *csx_generated_code_start : Please do not remove unless cleaning project
*set star_names[0] "Sol"
*set star_names[1] "York"
*set star_names[2] "Boyd"
*set star_names[3] "Ivan"
*set star_names[4] "Reef"
*set star_names[5] "Hook"
*set star_names[6] "Stan"
*set star_names[7] "Task"
*set star_names[8] "Sink"
*set star_names[9] "Sand"
*set star_names[10] "Quin"
*set star_names[11] "Gaol"
*set star_names[12] "Kirk"
*set star_names[13] "Kris"
*set star_names[14] "Fate"
*comment : *csx_generated_code_end : Please do not remove unless cleaning project

*set i 1

*label isn_rnd_loop
*rand rnd i 14
*set t star_names[i]
*set star_names[i] star_names[rnd]
*set star_names[rnd] t
*set i +1

*if (i <= 13)
    *goto isn_rnd_loop

*return
